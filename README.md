# Multisensor Plate

A simple PCB design with a CO2, Temperature and Humidity Sensor connected to
a Wemos D1 Mini.

## Parts

 * Wemos D1 Mini
 * MH-Z19 CO2 Sensor
 * DHT22 Temperature & Humidity Sensor

## Kicad Libs

 * [Wemos D1 Mini](https://github.com/rubienr/wemos-d1-mini-kicad.git)
 * [MH-Z19](https://gitlab.com/flyfloh/mh-z19)
